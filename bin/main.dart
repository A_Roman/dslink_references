import 'dart:async';
import 'dart:io';
import 'dart:convert';

import 'package:dslink/dslink.dart';
import 'package:dslink/nodes.dart';
import 'package:http/http.dart' as http;

LinkProvider link;
Requester requester;
SimpleNodeProvider provider;

var out = new File('logger.txt').openWrite();

Map<String,Timer> postingTimers = {};

main(List<String> args) {

  var profiles = {
    'addReference' : (String path) =>
    new SimpleActionNode(path, (Map<String,dynamic>params) async {

      //get the input values
      //The following wont be editable
      String referencePath =
        params['referencePath'];
      String nodeName =
        params['referenceName'];

      // the following are values;
      bool post =
          params['post'].toString().toLowerCase() == 'true';

      //the following will be attributes
      String targetUrl =
        params['targetUrl'].toString().toLowerCase();
      String userKey =
        params['userKey'].toString().toLowerCase();
      String deviceKey =
        params['deviceKey'].toString().toLowerCase();


      RemoteNode rNode = await requester.getRemoteNode(referencePath);

      if (rNode == null){
        return {
          "message": "Invalid reference path"
        };
      }

      // what is added in this node to get values
      var defaultCreatedNodes = {
        //node defs
        r'$is'              : 'referenceNode',
        r'$name'            : nodeName,
        '@referencePath'    : referencePath,        //this wont be changeable

        //editable string attributes
        '@targetUrl'        : targetUrl,
        '@userKey'          : userKey,
        '@deviceKey'        : deviceKey,

        //values
        'posting' : {
          r'$is'            : 'ifPosting',
          r'$name'          : 'Posting State',
          r'$type'          : 'bool',
          '?value'          : false
        },
        'postEvery' : {
          r'$is'            : 'postingInterval',
          r'$name'          : 'Post Every n Secs',
          r'$type'          : 'number',
          '?value'          : null
        },

        //editable strings are edited
        'edit' :{
          r'$name'          : 'Edit',
          r'$invokable'     : 'write',
          r'$params' : [
            {
              'name' : 'targetUrl',
              'type' : 'string',
//              'default': targetUrl
            },{
              'name' : 'userKey',
              'type' : 'string',
//              'default': userKey
            },{
              'name' : 'deviceKey',
              'type' : 'string',
//              'default': deviceKey
            }
          ],
          r'$result' : 'values',
          r'$columns'       : [
            {
              'name'        : 'message',
              'type'        : 'string'
            }
          ],
          r'$is'            : 'editAttributeStrings'
        },

        //call to populate data
        'rePopulate'  :{
          r'$name'          : 'RePopulate From Source',
          r'$invokable'     : 'write',
          r'$result'        : 'values',
          r'$columns'       : [
            {
              'name'        : 'message',
              'type'        : 'string'
            }
          ],
          r'$is'            : 'rePopulate'
        },

        //test posting, will possibly display full target url and data to post
        'testPost'          : {
          r'$name'          : 'Test Post',
          r'$invokable'     : 'write',
          r'$result'        : 'values',
          r'$columns'       : [
            {
              'name'        : 'statusCode',
              'type'        : 'string'
            },
            {
              'name'        : 'requestBody',
              'type'        : 'string'
            }
          ],
          r'$is'            : 'testPost',
        },

        'postProgramattically' : {
          r'$name'          : 'Post Programmatically',
          r'$invokable'     : 'write',
          r'$result'        : 'values',
          r'$columns'       : [
            {
              'name'        : 'mesage',
              'type'        : 'string'
            }
          ],
          r'$params'         : [
            {
              'name'        : 'postEvery',
              'type'        : 'number'
            }
          ],
          r'$is'            : 'postEvery'
        },

        'stopPosting'       : {
          r'$name'          : 'Stop Posting',
          r'$invokable'     : 'write',
          r'$is'            : 'stopPosting'
        },

        'remove'            : {
          r'$name'          : 'Remove Reference',
          r'$is'            : 'remove',
          r'$invokable'     : 'write'
        }
      };

      link.addNode('/$nodeName', defaultCreatedNodes);

      PopulateData('/$nodeName', referencePath);

      link.save();

      return {
        "message": "Success! Reference created."
      };
    })

    ,'remove': (String path) =>
    new SimpleActionNode(path, (Map<String, dynamic> params) {

      var nodePath = new Path(path).parentPath;
      var node = link.getNode(nodePath);
      link.removeNode(nodePath);

      link.save();

      if (postingTimers.keys.contains([node.get(r'$name')])) {
        postingTimers.remove([node.get(r'$name')]);
      }


    },provider)

    ,'postEvery' : (String path) =>
    new SimpleActionNode(path, (Map<String, dynamic> params) async {

      var nodePath = new Path(path).parentPath;

      String data = getData(provider.getNode(nodePath));

      bool isPosting = link.val('$nodePath/posting') == true;

      if (isPosting) {
        return{
          "message": "Already Posting!"
        };
      }

      var lNode = provider.getNode(nodePath);
      num postEvery = num.parse(params['postEvery']);
      link.val('$nodePath/postEvery', postEvery);

      link.val('$nodePath/posting', true);

      HttpRequestOnTimer(lNode);

//      print(response.body);

    },provider)

    ,'stopPosting' : (String path) =>
    new SimpleActionNode(path, (Map<String, dynamic> params) async {

      var nodePath = new Path(path).parentPath;

//  String data = getData(provider.getNode(nodePath));

      var lNode = provider.getNode(nodePath);

      link.val('$nodePath/posting', false);

      link.val('$nodePath/postEvery', null);

      if (postingTimers.containsKey(lNode.get(r'$name'))){
        postingTimers.remove([lNode. get (r'$name')]);
      }

      link.saveAsync();

    },provider)

    ,'testPost' : (String path) =>
    new SimpleActionNode(path, (Map<String, dynamic> params) async {

      var nodePath = new Path(path).parentPath;

      String data = getData(provider.getNode(nodePath));

      var lNode = provider.getNode(nodePath);
      String targetUrl = lNode.getAttribute('@targetUrl');
      String deviceKey = lNode.getAttribute('@deviceKey');
      String userKey = lNode.getAttribute('@userKey');

      //the reponse is a httpresponse.... body, statuscode and the like
      http.Response response = await HttpPostCall(targetUrl, data, userKey, deviceKey);

//      print(response.body);

      return{
        'statusCode' : response.statusCode.toString(),
        'requestBody' : data
      };

    },provider)

    ,'editAttributeStrings' : (String path) =>
    new SimpleActionNode(path, (Map<String, dynamic> params) async {
      var nodePath = new Path(path).parentPath;
      var lNode = provider.getNode(nodePath);

      params.forEach((attributeName,newStringValue) {

        params[attributeName].toString().toLowerCase();
        lNode.attributes['@$attributeName'] = newStringValue;
        lNode.listChangeController.add('@$attributeName');

      });

      link.save();

      return {
        'message': 'Changes applied!'
      };
    })

    ,'rePopulate' : (String path) =>
    new SimpleActionNode(path, (Map<String, dynamic> params) {

      String nodePath = new Path(path).parentPath;
      var lNode = provider.getNode(nodePath);
      String referencePath = lNode.getAttribute('@referencePath');


      PopulateData(nodePath, referencePath);
      link.save();

      return{
        'message' : 'Node repopulated, all values set to source defaults'
      };

    })

    ,'editValuedName' : (String path) =>
    new SimpleActionNode(path, (Map<String, dynamic> params) async {

      String nodePath = new Path(path).parentPath;
      var lNode = provider.getNode(nodePath);

      String sourcePath =  lNode.getAttribute('@sourcePath');
      String type = lNode.getConfig(r'$type');
      String newName = params['newName'].toString();

      var pathParts = nodePath.split('/');
      pathParts[pathParts.length-1] = newName;
      var newPath = pathParts.join('/');

      provider.removeNode(nodePath);
      AddOneValuedPath(type, sourcePath, newName, newPath);

      SubscribeToOneSource(newPath,sourcePath);

      link.saveAsync();

    },provider)

    ,'changePosting' : (String path) =>
    new SimpleActionNode(path, (Map<String, dynamic> params) async {
      var nodePath = new Path(path).parentPath;

      LocalNode lNode = provider.getNode(nodePath);

      var boolCurrentValue = lNode.getAttribute('@postInclude').toString() == 'true';
      var boolCoV = lNode.getAttribute('@CoV').toString() == 'true';

      if (!boolCoV) {
        lNode.attributes['@postInclude'] = !boolCurrentValue;
        lNode.listChangeController.add('@postInclude');
      }

      link.saveAsync();

    },provider)

    ,'toggleAttributeBool'   : (String path) =>
    new SimpleActionNode(path, (Map<String, dynamic> params)  {

      var nodePath = new Path(path).parentPath;
      var lNode = provider.getNode(nodePath);
      var boolAttribute = provider.getNode(path).get(r'$name').toString().split(' ')[1];

      bool currentBoolValue = lNode.attributes['@$boolAttribute'] == true;
      lNode.attributes['@$boolAttribute'] = !currentBoolValue;
      lNode.listChangeController.add('@$boolAttribute');

      bool pointPostingState = lNode.get('@postInclude') == true;

      if (boolAttribute == 'CoV' && pointPostingState) {
        lNode.attributes['@postInclude'] = false;
        lNode.listChangeController.add('@postInclude');
      }

      link.save();
    })


  };

  var nodes = {
    'AddReference' :
    {
      r'$name' : 'Add Reference',
      r'$invokable' : 'write',
      r'$params' : [
        {
          'name' : 'referencePath',
          'type' : 'string'
        },{
          'name' : 'referenceName',
          'type' : 'string',
          'default' : 'reference'
        },{
          'name'  : 'targetUrl',
          'type'  : 'string'
        },{
          'name'  : 'userKey',
          'type'  : 'string'
        },{
          'name'  : 'deviceKey',
          'type'  : 'string'
        }
      ],
      r'$result' : 'values',
      r'$columns' : [
        {
          'name' : 'message',
          'type' : 'string'
        }
      ],
      r'$is' : 'addReference',
    }
  };

  link = new LinkProvider(args, 'Referencer-',
      isRequester: true,
      isResponder: true,
      defaultNodes: nodes,
      profiles: profiles,
      encodePrettyJson: true
  );

  requester = link.requester;
  provider = link.provider;

  link.init();
  link.connect();

  ReEstablishSubscriptions();


}

PopulateData(String path, String referencePath) async {


  RemoteNode referenceNode = await requester.getRemoteNode(referencePath);

  //print(referenceNode.getSimpleMap());
  List<String> valuedPaths = [];

  var sourcePaths = await IterateRemoteChildren(referenceNode, valuedPaths);
  var newPathsWithSources = await AddValuedPaths(path,sourcePaths); // now returns a map<new path;source path>

  //print(sourcePaths);
  SubscribeToSources(newPathsWithSources);

}

Future<List<String>> IterateRemoteChildren(RemoteNode targetNode, List<String> valuedPaths) async {

  // get only values that we car about
  var acceptable = ['num','int','bool','number'];
  //found appropriate value
  if (acceptable.contains(targetNode.getConfig(r'$type'))) {
    valuedPaths.add(targetNode.remotePath);
  }
  //found a node with children, iterate through it
  if (targetNode.children.isNotEmpty) {
    for (var nodeName in targetNode.children.keys) {
      var newTargetPath = targetNode.remotePath + '/$nodeName';
      try {
        IterateRemoteChildren(await requester.getRemoteNode(newTargetPath),valuedPaths);
      } catch (e) {
        print('Log:: Unable to iterate over {$newTargetPath}');
      }

    }
  }

  return valuedPaths;
}

Future<Map<String,String>> AddValuedPaths(String path, List<String> valuedPaths) async {

  // {new path; source path }
  Map<String,String> newPathsWithSources = {};

  valuedPaths.forEach( (sourcePath) async {
    try{
      RemoteNode valuedRNode = await requester.getRemoteNode(sourcePath);
      String name = valuedRNode.name;
      String type = valuedRNode.getConfig(r'$type');

      String newPath = '$path/$name';

      newPathsWithSources[newPath] = sourcePath;

      AddOneValuedPath(type, sourcePath, name, newPath);
    } catch (e) {
      print('Log:: Unable to add over {$sourcePath}');
    }
  });

//  for(String sourcePath in valuedPaths){
//    try{
//      RemoteNode valuedRNode = await requester.getRemoteNode(sourcePath);
//      String name = valuedRNode.name;
//      String type = valuedRNode.getConfig(r'$type');
//
//      String newPath = '$path/$name';
//
//      newPathsWithSources[newPath] = sourcePath;
//
//      AddOneValuedPath(type, sourcePath, name, newPath);
//    } catch (e) {
//      print('Log:: Unable to add over {$sourcePath}');
//    }
//  }
  // list required for subscribers
  return newPathsWithSources;
}

AddOneValuedPath(String type, String sourcePath, String name, String newPath) {

  var createdValuedNode = {
    //node defs
    r'$is'              : 'referencedValue',
    r'$type'            : type,
    '?value'            : null,

    '@postInclude'      : true,
    '@sourcePath'       : sourcePath,
//    '@Step'           : false,
    '@CoV'              : false,

    'remove' : {
      r'$name'          : 'Remove Datapoint',
      r'$is'            : 'remove',
      r'$invokable'     : 'write'
    },


    'editName' :{
      r'$name'          : 'Edit PostingName',
      r'$invokable'     : 'write',
      r'$params' : [
        {
          'name' : 'newName',
          'type' : 'string',
          'default': name
        }
      ],
      r'$is'            : 'editValuedName'
    },

    'changePosting' : {
      r'$name'          : 'Change Posting Status',
      r'$invokable'     : 'write',
      r'$is'            : 'changePosting'

    },
//
    'toggleCoV' : {
      r'$name'          : 'Toggle CoV',
      r'$invokable'     : 'write',
      r'$is'            : 'toggleAttributeBool'

    },
//
//    'toggleStep' : {
//      r'$name'          : 'Toggle Step',
//      r'$invokable'     : 'write',
//      r'$is'            : 'toggleAttributeBool'
//
//    }
  };
  provider.addNode(newPath, createdValuedNode);

  link.save();

}

SubscribeToSources(Map<String,String> newPathsWithSources){

  newPathsWithSources.forEach( (newPath, sourcePath) {
    SubscribeToOneSource(newPath, sourcePath);
  });

}

SubscribeToOneSource(String newPath, String sourcePath) {

  LocalNode updatingNode = provider.getNode('$newPath');

  requester.subscribe(sourcePath,
      (ValueUpdate update) {

        if (updatingNode.get('@CoV')) {
//          print('CoV in sub');
          HttpRequestOnCoV(updatingNode, update.value);
        }

        updatingNode.updateValue(update.value);
      }
  );

}

CoVPost(LocalNode lNode) async {

}

String getData(LocalNode node) {
  String data = '';
  bool first = true;

  for (var childNodeName in node.children.keys){

    String childPath = '${node.path}/$childNodeName';
    SimpleNode sNode = provider.getNode(childPath);

    if (sNode.get(r'$is') == 'referencedValue'){

      var valueNode = provider.getNode('${sNode.path}');

      String postValue = valueNode.value.toString();
      bool postInclude =
        valueNode.getAttribute('@postInclude').toString() == 'true';

      if (postInclude == true && first == true ) {
        data = '$childNodeName=$postValue';
        first = false;
      }
      else if (postInclude == true && first==false ){
        data = data + '&$childNodeName=$postValue';
      };
    };
  }
  return data;
}

ReEstablishSubscriptions() {
  //iterate over root
  for(var possibleRefNode in provider.getNode('/').children.keys){

    LocalNode lNodeRootChild = provider.getNode('/$possibleRefNode');

    //find referenceNodes
    if(lNodeRootChild.get(r'$is') == 'referenceNode'){

      //iterate over children
      for(var referenceChildren in lNodeRootChild.children.keys){

        LocalNode lNodeReferenceChild = provider.getNode('/$possibleRefNode/$referenceChildren');

        if (lNodeReferenceChild.get(r'$is') == 'referencedValue'){

          String sourcePath = lNodeReferenceChild.get('@sourcePath');
          SubscribeToOneSource('/$possibleRefNode/$referenceChildren',sourcePath);

        }
      }
    }
  }
}

Future HttpPostCall(String postUrl, String data, String userKey, String deviceKey) async {

  Map<String,String> headers = {
    'connector' : 'DGBOX2EMCORE',
    'userKey'   : userKey,
    'deviceKey' : deviceKey
  };

  var response = await http.post(Uri.parse(postUrl),headers: headers,body: data);

  return response;
}


HttpRequestOnCoV(LocalNode lNodeDataPoint, dynamic valueUpdate) async {

  String referencePath = new Path(lNodeDataPoint.path).parentPath;
  LocalNode lNodeReference = link.getNode(referencePath);

  String postUrl = lNodeReference.getAttribute('@targetUrl');
  String userKey = lNodeReference.getAttribute('@userKey');
  String deviceKey = lNodeReference.getAttribute('@deviceKey');

  bool error = false;

  if (postUrl == 'null' || postUrl == 'null' || postUrl == 'null') {
    error = true;
  }

  String data = '${lNodeDataPoint.path.split('/').last}=${valueUpdate.toString()}';

  if (!error) {
    try {
      HttpPostCall(postUrl, data, userKey, deviceKey).then( (http.Response response){
//        print('CoV response ${response.statusCode} :: $data');
      });
    } catch (e) {
      print('Error :: {HTTP on CoV} - $e');
    }
  }

}


HttpRequestOnTimer(LocalNode lNodeReference) {

  print('Log :: starting to post programmm....');
  bool posting;
  Timer httpTimer;

//  var out = new File('logger.txt').openWrite();

  String postUrl = lNodeReference.getAttribute('@targetUrl');
  String userKey = lNodeReference.getAttribute('@userKey');
  String deviceKey = lNodeReference.getAttribute('@deviceKey');

  LocalNode lNodePostEvery = provider.getNode('${lNodeReference.path}/postEvery');

  Duration seconds = new Duration(seconds: int.parse(lNodePostEvery.value.toString()));

  httpTimer = new Timer.periodic(seconds, (Timer t) async {

    posting = provider.getNode('${lNodeReference.path}/posting').value == 'true';

    if (posting) {
      httpTimer.cancel();
    }

    String data = getData(lNodeReference);

    String log = '';

    String now = new DateTime.now().toIso8601String();
    try{
      http.Response response = await HttpPostCall(postUrl, data, userKey, deviceKey);
      log = 'Status Code ${response.statusCode} at {${now}} to user {$userKey} and asset {$deviceKey}';
    } catch (e) {
      log = 'Error :: ($e) for time $now';
    }
    print('Log :: $log');

  });

  postingTimers['${lNodeReference.get(r'$name')}'] = httpTimer;
}
